# MQTT-STUFF

## Quality of Service Levels

    - QoS 0 (At most once):
        Nachricht wird einmal gesendet, ohne Bestätigung vom Empfänger.
        Kein erneuter Versand bei fehlgeschlagener Übertragung.
    - QoS 1 (At least once):
        Nachricht wird mindestens einmal gesendet.
        Empfänger bestätigt Empfang.
        Kann zu mehrfacher Zustellung führen, falls Bestätigung verloren geht.
    - QoS 2 (Exactly once):
        Nachricht wird genau einmal gesendet.
        Höchste Zuverlässigkeit durch mehrstufiges Handshake-Verfahren.
        Vermeidet doppelte Zustellungen.

## Persistent Session and Queuing Messages

    - Persistent Session:
        Sitzung bleibt erhalten, auch wenn Client offline geht.
        Nachrichten mit QoS 1 oder 2 werden in der Warteschlange gehalten, bis Client wieder online ist.
        Abonnements und unbestätigte Nachrichten werden gespeichert.
    - Queuing Messages:
        Nachrichten werden in einer Warteschlange gespeichert, bis sie an den Empfänger zugestellt werden können.
        Relevant für persistente Sitzungen und Clients, die zeitweise offline sind.

## Retained Messages

    - Retained Messages:
        Letzte Nachricht zu einem Thema wird gespeichert.
        Bei neuem Abonnement wird die gespeicherte Nachricht sofort an den Abonnenten gesendet.
        Nützlich für Statusupdates oder Initialisierungsdaten.

## Last Will and Testament (LWT)

    - Last Will and Testament (LWT):
        Nachricht, die vom Broker gesendet wird, wenn ein Client unerwartet die Verbindung verliert.
        Definiert beim Verbindungsaufbau.
        Informiert andere Clients über die unvorhergesehene Trennung des Clients.

## Keep Alive & Client Take-Over

    - Keep Alive:
        Zeitintervall, das der Client dem Broker angibt, um regelmäßig Lebenszeichen zu senden.
        Verhindert unerwünschte Trennung aufgrund von Inaktivität.
    - Client Take-Over:
        Ein neuer Verbindungsaufbau eines Clients mit derselben Client-ID übernimmt die bestehende Verbindung.
        Alte Verbindung wird getrennt.
        Nützlich, um sicherzustellen, dass nur eine Instanz eines Clients aktiv ist.


## Vorteile von MQTT

    - Leichtgewichtig:
        Minimale Overhead-Daten.
        Geringer Bandbreitenbedarf.
        Ideal für ressourcenbeschränkte Geräte.

    - Effizienter Energieverbrauch:
        Wenig Rechenleistung und Energie erforderlich.
        Geeignet für batteriebetriebene Geräte.

    - Asynchronität:
        Geräte müssen nicht gleichzeitig online sein.
        Nachrichten werden zwischengespeichert und später zugestellt.

    - Skalierbarkeit:
        Unterstützt viele Clients und hohe Nachrichtenzahlen.
        Belastbar für IoT-Implementierungen.

    - Qualität der Dienstebenen (QoS):
        Verschiedene QoS-Stufen bieten flexible Zuverlässigkeit.
        Balanciert zwischen Latenz und Datenintegrität.

    - Retained Messages:
        Speichert die letzte Nachricht eines Themas.
        Neue Abonnenten erhalten sofort aktuelle Informationen.

    - Last Will and Testament (LWT):
        Informiert andere Clients über unvorhergesehene Verbindungsverluste.
        Erhöht Zuverlässigkeit und Transparenz.

    - Klein und einfach:
        Einfach zu implementieren und zu verwalten.
        Unterstützt durch viele Bibliotheken und Tools.

    - Sicherheitsoptionen:
        Unterstützt SSL/TLS für sichere Übertragungen.
        Authentifizierung und Autorisierung möglich.

    - Verbindungsstatusüberwachung:
        Keep-Alive-Meldungen zur Überwachung der Verfügbarkeit von Clients.
        Schnelle Erkennung und Reaktion auf Verbindungsverluste.

## Nachteile von MQTT

    - Sicherheitsrisiken:
        Standardmäßig unverschlüsselt (außer bei Nutzung von SSL/TLS).
        Anfällig für Man-in-the-Middle-Angriffe ohne entsprechende Sicherheitsmaßnahmen.

    - QoS Overhead:
        Höhere QoS-Stufen verursachen mehr Overhead.
        Kann Bandbreite und Ressourcenverbrauch erhöhen.

    - Skalierbarkeit:
        Broker kann ein Engpass sein.
        Bei vielen Clients oder hohen Datenmengen kann es zu Leistungsproblemen kommen.

    - Abhängigkeit vom Broker:
        Single Point of Failure (SPOF).
        Broker-Ausfall beeinträchtigt das gesamte Kommunikationsnetzwerk.

    - Limitierte Offline-Fähigkeiten:
        Clients müssen sich regelmäßig verbinden, um Nachrichten zu empfangen.
        Offline-Speicherung und Synchronisation sind begrenzt.

    - Fehlende Standardisierung:
        Unterschiedliche Implementierungen und Erweiterungen können Kompatibilitätsprobleme verursachen.
        Nicht alle Clients und Broker unterstützen dieselben Funktionen.

    - Nachrichten-Größe:
        MQTT ist nicht ideal für sehr große Nachrichten.
        Beschränkungen in der Broker-Konfiguration möglich.

    - Debugging und Überwachung:
        Kann kompliziert sein, da MQTT-Protokoll leichtgewichtig und binär ist.
        Fehlersuche und Überwachung erfordern spezialisierte Tools und Kenntnisse.

    - Benachrichtigungsverzögerung:
        Bei schlechtem Netzwerk oder hoher Last können Verzögerungen auftreten.
        Echtzeit-Anforderungen möglicherweise nicht immer erfüllbar.

    - Einrichtungskomplexität:
        Sichere Konfiguration und Verwaltung kann komplex sein.
        Notwendigkeit für Fachwissen in Netzwerk- und Sicherheitskonfigurationen.