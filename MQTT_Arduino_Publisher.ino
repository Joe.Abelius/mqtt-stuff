// Im Boards Manager muss das Paker "Arduino megaAVR Boards" installiert werden.
// Im Library Manager müssen die Pakete "ArduinoMqttClient" und "WiFiNINA" installiert werden.
// Die Datei "arduino_secrets.h" wird über die drei Punkte oben rechts angelegt("New Tab"). Dort wird die SSID und das Passwort des Netzwerks angegeben. Die Datei sieht dann wie folgt aus(ansonsten siehe arduino_secrets Datei):
// #define SECRET_SSID "Wlan-Name"
// #define SECRET_PASS "Passwort" 
// Das derzeit verwendete Board muss zusätslich ausgewählt werden.
// Der Code wird erst kompiliert und dann geuploaded


#include <ArduinoMqttClient.h> // Paket muss aus der Library installiert werden
#include <WiFiNINA.h> // Paket muss aus der Library installiert werden
#include "arduino_secrets.h" // Datei muss zusätslich erstellt werden
// Bitte geben Sie Ihre sensiblen Daten in der Datei arduino_secrets.h ein
char ssid[] = SECRET_SSID;        
WiFiClient wifiClient;
MqttClient mqttClient(wifiClient);
const char broker[] = "broker.hivemq.com"; // <-- Broker Namen oder IP eintragen
int        port     = 1883; // <-- Port des Brokers einfügen ; 1883 = ungeschütze Übertragung ; 8883 = TLS/SSL Übertragung
const char topic[]  = "Fantastisches_random_topic";  // Hier wird der Topic Name eingetragen
const char topic2[] = "Fantastisches_random_topic2"; // Hier wird der Topic Name eingetragen
const char topic3[] = "Fantastisches_random_topic3"; // Hier wird der Topic Name eingetragen
// Intervall zum Senden von Nachrichten (Millisekunden)
const long interval = 12000;
unsigned long previousMillis = 0;
void setup() {
  // Initialisiere die serielle Schnittstelle und warte, bis der Port geöffnet wird:
  Serial.begin(9600);
  while (!Serial) {
    ; // Warte auf die serielle Verbindung. Nur für native USB-Ports benötigt
  }
  // Versuche, eine Verbindung zum WLAN-Netzwerk herzustellen:
  Serial.print("Versuche, eine Verbindung zum WPA-SSID herzustellen: ");
  Serial.println(ssid);
  while (WiFi.begin(ssid, pass) != WL_CONNECTED) {
    // Fehlgeschlagen, erneut versuchen
    Serial.print(".");
    delay(5000);
  }
  Serial.println("Verbunden mit dem Netzwerk");
  Serial.println();
  Serial.print("Versuche, eine Verbindung zum MQTT-Broker herzustellen: ");
  Serial.println(broker);
  if (!mqttClient.connect(broker, port)) {
    Serial.print("MQTT-Verbindung fehlgeschlagen! Fehlercode = ");
    Serial.println(mqttClient.connectError());
    while (1);
  }
  Serial.println("Verbunden mit dem MQTT-Broker!");
  Serial.println();
}
void loop() {
  // poll() regelmäßig aufrufen, um das Senden von MQTT-keep-alive zu ermöglichen
  // Vermeiden der Trennung durch den Broker
  mqttClient.poll();
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // Speichere die letzte Zeit, zu der eine Nachricht gesendet wurde
    previousMillis = currentMillis;
    // Vordefinierte Zeichenketten oder Zahlen
    const char message1[] = "Hello, Test123!";
    const char message2[] = "MQTT Message 2";
    int message3 = 12345;

    Serial.print("Sende Nachricht an Thema: ");
    Serial.println(topic);
    Serial.println(message1);
    Serial.print("Sende Nachricht an Thema: ");
    Serial.println(topic2);
    Serial.println(message2);
    Serial.print("Sende Nachricht an Thema: ");
    Serial.println(topic3);
    Serial.println(message3);
    // Nachricht senden, das Print-Interface kann verwendet werden, um den Nachrichtentext festzulegen
    mqttClient.beginMessage(topic, true, 2); // Retained, QoS 2
    mqttClient.print(message1);
    mqttClient.endMessage();
    mqttClient.beginMessage(topic2, true, 2); // Retained, QoS 2
    mqttClient.print(message2);
    mqttClient.endMessage();
    mqttClient.beginMessage(topic3, true, 2); // Retained, QoS 2
    mqttClient.print(message3);
    mqttClient.endMessage();
    Serial.println();
  }
}

